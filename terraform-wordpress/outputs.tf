# outputs.tf

output "rds_endpoint" {
  description = "The endpoint of the RDS instance"
  value       = aws_db_instance.wordpressdb.endpoint
}

output "ec2_public_ip" {
  description = "The public IP of the EC2 instance"
  value       = aws_instance.wordpress.public_ip
}
