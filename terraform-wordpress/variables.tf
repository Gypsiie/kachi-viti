variable "database_user" {
  description = "The username for RDS database"
  default     = "dbuser" // Replace with your db_username
}

variable "database_password" {
  description = "The password for RDS database"
  default     = "dbpassword" // Replace with your db_password
}

variable "database_name" {
  description = "The name of RDS database"
  default     = "wordpressdb" // replace with your database name
}

variable "vpc_id" {
  description = "The ID of my prod VPC"
  default     = "vpc-vpc_id" // Replace with your VPC_ID
}
