# In main.tf

# Define Security Group for EC2 instance
resource "aws_security_group" "ec2_sg" {
  name        = "ec2_sg"
  description = "Allow inbound traffic for ssh and http"
  vpc_id      = "vpc-vpc_id" # Replace with your VPC ID

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["x.x.x.x/32"] # Replace with your IP address
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] # Allows HTTP traffic from everywhere
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Define RDS instance
resource "aws_db_instance" "wordpressdb" {
  identifier_prefix   = "wordpressdb"
  engine              = "mysql"
  engine_version      = "5.7"  # Use the version compatible with your application
  instance_class      = "db.t2.micro" # Adjust based on needs
  allocated_storage   = 20  # Adjust based on needs
  username            = var.database_user
  password            = var.database_password
  publicly_accessible = false  # Keep DB private

  vpc_security_group_ids = [aws_security_group.ec2_sg.id]
  db_subnet_group_name = "prod-db-subnet-group"
}

# Fetch the latest Amazon Linux 2 AMI
data "aws_ami" "amazon_linux_2" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-gp2"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }
}

# Define EC2 instance
resource "aws_instance" "wordpress" {
  ami                         = data.aws_ami.amazon_linux_2.id
  instance_type               = "t2.micro"  # Choose instance type
  vpc_security_group_ids      = [aws_security_group.ec2_sg.id]
  subnet_id                   = "subnet-SubnetID" 
  associate_public_ip_address = true
  user_data                   = templatefile("${path.module}/userdata.sh", {
    db_username      = var.database_user
    db_user_password = var.database_password
    db_name          = var.database_name
    db_RDS           = aws_db_instance.wordpressdb.endpoint
  })
  key_name                    = "KeyPair" # your key pair name
  tags = {
    Name = "wordpress-instance"
  }
}
