terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.9.0" // or higher, depending on your needs
    }
  }
  backend "s3" {
    bucket = "terraform-state-kachi"
    key    = "terraform-state/terraform.tfstate"
    region = "us-east-1"
  }
}

provider "aws" {
  region = "us-east-1"
}
